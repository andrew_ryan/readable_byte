## readable_byte
[![crates.io](https://img.shields.io/crates/v/systemstat.svg)](https://crates.io/crates/readable_byte)
[![API Docs](https://docs.rs/systemstat/badge.svg)](https://docs.rs/readable_byte/)
[![unlicense](https://img.shields.io/badge/un-license-green.svg?style=flat)](https://unlicense.org)


readable_byte is an implmentation of human-readable bytesize


## Usage

Add this to your Cargo.toml:

```toml
[dependencies]
readable_byte="0.1.0"
```

## Example
### Human readable representations
```rust
    fn assert_to_string_u64(expected: &str, b: readable_byte, si: bool) {
        assert_eq!(expected.to_string(), b.to_string_as(si));
    }

    #[test]
    fn test_to_string_as() {
        assert_to_string_u64("215 B", readable_byte::b(215), true);
        assert_to_string_u64("215 B", readable_byte::b(215), false);

        assert_to_string_u64("1.0 kiB", readable_byte::kib(1), true);
        assert_to_string_u64("1.0 KB", readable_byte::kib(1), false);

        assert_to_string_u64("293.9 kiB", readable_byte::kb(301), true);
        assert_to_string_u64("301.0 KB", readable_byte::kb(301), false);

        assert_to_string_u64("1.0 MiB", readable_byte::mib(1), true);
        assert_to_string_u64("1048.6 KB", readable_byte::mib(1), false);

        assert_to_string_u64("1.9 GiB", readable_byte::mib(1907), true);
        assert_to_string_u64("2.0 GB", readable_byte::mib(1908), false);

        assert_to_string_u64("399.6 MiB", readable_byte::mb(419), true);
        assert_to_string_u64("419.0 MB", readable_byte::mb(419), false);

        assert_to_string_u64("482.4 GiB", readable_byte::gb(518), true);
        assert_to_string_u64("518.0 GB", readable_byte::gb(518), false);

        assert_to_string_u64("741.2 TiB", readable_byte::tb(815), true);
        assert_to_string_u64("815.0 TB", readable_byte::tb(815), false);

        assert_to_string_u64("540.9 PiB", readable_byte::pb(609), true);
        assert_to_string_u64("609.0 PB", readable_byte::pb(609), false);
    }
    #[test]
    fn test_default() {
        assert_eq!(readable_byte::b(0), readable_byte::default());
    }

    #[test]
    fn test_to_string_u64() {
        assert_to_string_u64("88.0 PB", readable_byte::pb(88), false);
    }
  #[test]
  fn test_parsing_from_str() {
      // shortcut for writing test cases
      fn parse(s: &str) -> u64 {
          s.parse::<readable_byte>().unwrap().0
      }

      assert_eq!("0".parse::<readable_byte>().unwrap().0, 0);
      assert_eq!(parse("0"), 0);
      assert_eq!(parse("500"), 500);
      assert_eq!(parse("1K"), Unit::KiloByte * 1);
      assert_eq!(parse("1Ki"), Unit::KibiByte * 1);
      assert_eq!(parse("1.5Ki"), (1.5 * Unit::KibiByte) as u64);
      assert_eq!(parse("1KiB"), 1 * Unit::KibiByte);
      assert_eq!(parse("1.5KiB"), (1.5 * Unit::KibiByte) as u64);
      assert_eq!(parse("3 MB"), Unit::MegaByte * 3);
      assert_eq!(parse("4 MiB"), Unit::MebiByte * 4);
      assert_eq!(parse("6 GB"), 6 * Unit::GigaByte);
      assert_eq!(parse("4 GiB"), 4 * Unit::GibiByte);
      assert_eq!(parse("88TB"), 88 * Unit::TeraByte);
      assert_eq!(parse("521TiB"), 521 * Unit::TebiByte);
      assert_eq!(parse("8 PB"), 8 * Unit::PetaByte);
      assert_eq!(parse("8P"), 8 * Unit::PetaByte);
      assert_eq!(parse("12 PiB"), 12 * Unit::PebiByte);
  }
```

